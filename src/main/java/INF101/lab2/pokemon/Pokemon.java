package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    //class field variables
    public String name;
    public int healthPoints;
    public int maxHealthPoints;
    public int strength;
    Random random;

    //class constructor, the constructor has the same name as the object it belongs to object
    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
        System.out.println(toString());
    }

    //class methods
    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;
    }

    public boolean isAlive() {
        return healthPoints > 0;
        }

    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());

        //cannot inflige negative damages (-+- is +)
        if (damageInflicted < 0){
            damageInflicted = 0;
        }

        target.damage(damageInflicted);

        System.out.println(this.name + " attacks " + target.getName());
        System.out.println(target.getName() + " takes " + damageInflicted + " damage and is left with " + target.getCurrentHP() + "/" + target.getMaxHP() + " HP");

        if (target.isAlive() == false){
            System.out.println(target.getName() + " is defeated by " + this.name);
        }
    }

    @Override
    public void damage(int damageTaken) {
        //cannot inflige negative damages (-+- is +)
        if (damageTaken >= 0){
            this.healthPoints -= damageTaken;
            if (this.healthPoints < 0){
                this.healthPoints = 0;
            }
        }
    }

    @Override
    public String toString() {
        return this.name + " HP: (" + this.healthPoints + "/" + this.maxHealthPoints + ") STR: " + this.strength;
    }

}